<?php

namespace Drupal\deploy_block_content\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\deploy_block_content\Service\BlockContentExporter;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class ExportBlockContentForm.
 *
 * @package Drupal\deploy_block_content\Form
 */
class ExportBlockContentForm extends FormBase {

  /**
   * The block content exporter.
   *
   * @var \Drupal\deploy_block_content\Service\BlockContentExporter
   */
  private $exporter;

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  private $request;

  /**
   * The drupal translation service.
   *
   * @var \Drupal\Core\StringTranslation\TranslationInterface
   */
  private $translation;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get(BlockContentExporter::SERVICE_ID),
      $container->get('request_stack'),
      $container->get('string_translation')
    );
  }

  /**
   * ExportBlockContentForm constructor.
   *
   * @param \Drupal\deploy_block_content\Service\BlockContentExporter $exporter
   *   The block content exporter.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack to get the current request.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $translation
   *   The drupal translation service.
   */
  public function __construct(BlockContentExporter $exporter, RequestStack $request_stack, TranslationInterface $translation) {
    $this->exporter = $exporter;
    $this->request = $request_stack->getCurrentRequest();
    $this->translation = $translation;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Detect the block content.
    $block_content = $this->request->attributes->get('block_content');

    $form['export_data'] = [
      '#type' => 'textarea',
      '#title' => $this->translation->translate('Exported Block Content'),
      '#default_value' => $this->exporter->exportBlock($block_content),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // TODO: Implement submitForm() method.
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'export_block_content_form';
  }


}
