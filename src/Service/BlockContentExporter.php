<?php

namespace Drupal\deploy_block_content\Service;

use Drupal\block_content\BlockContentInterface;

/**
 * Class BlockContentExporter.
 *
 * @package Drupal\deploy_block_content\Service
 */
class BlockContentExporter {

  const SERVICE_ID = 'deploy_block_content.block_content_exporter';

  /**
   * Exports the block content data.
   *
   * @param \Drupal\block_content\BlockContentInterface $block_content
   *   The block content to export the data from.
   *
   * @return array
   *   An array containing the exported data.
   */
  public function exportBlock(BlockContentInterface $block_content) {
    $entity_type_id = $block_content->getEntityTypeId();

    return [
      $entity_type_id => [
        $block_content->getConfigTarget() => array_merge(
          [
            'info' => $block_content->get('info')->getString(),
            'type' => $entity_type_id,
          ],
          $this->getAllFields($block_content)
        ),
      ],
    ];
  }

  /**
   * Returns all fields which name starts with "field_".
   *
   * @param \Drupal\block_content\BlockContentInterface $block_content
   *   The block content entity to return the fields from.
   *
   * @return array
   *   An array of the fields.
   */
  private function getAllFields(BlockContentInterface $block_content) {
    $fields = [];
    foreach ($block_content->getFields() as $field_name => $field_item) {
      if (strstr($field_name, 'field_')) {
        $fields[$field_name] = $field_item->getString();
      }
    }

    return $fields;
  }

}
